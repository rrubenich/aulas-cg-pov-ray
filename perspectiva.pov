#version 3.7;
global_settings { assumed_gamma 1 }

#include "colors.inc"    // The include files contain
#include "stones.inc"    // pre-defined scene elements
#include "textures.inc"  // pre-defined scene elements    
#include "shapes.inc"               

camera {
  location <15, 15, 15>
  look_at <0, 0, 0>
}

light_source { 
  <20, 7, 7> 
  color Blue 
  spotlight
}

light_source { 
  <7, 7, 20>
  color Yellow 
  spotlight  
}

cylinder {
  <8, 7, 7>,     // Center of one end
  <6, 7, 7>,     // Center of other end
  1           // Radius
  

  texture{
    pigment { color Yellow }
  }
}

plane { <1, 0, 0>, -1
    pigment {
      color Blue
    }
  }

plane { <0, 1, 0>, -1
    pigment {
      color Gray
    }
  }

plane { <0, 0, 1>, -1
    pigment {
      color Red
    }
  }
