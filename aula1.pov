#version 3.7;
global_settings { assumed_gamma 1 }

#include "colors.inc"    // The include files contain
#include "stones.inc"    // pre-defined scene elements
#include "textures.inc"  // pre-defined scene elements    
#include "shapes.inc" 
#include "glass.inc" 
#include "metals.inc" 
#include "woods.inc"                

camera {
  location <0, 20, -100>
  right     x*image_width/image_height
  angle 20
  look_at <0, 5, 0>
}

light_source { <30, 20, 0> color White}

background { color Blue } 


cylinder { 
  <0, 0, 0>, // Center of one end 
  <0, 10, 0>, // Center of other end 
  1 // Radius 
   // Remove end caps                                           
   texture{ T_Stone21   
                normal { agate 0.25 scale 0.15 rotate<0,0,0> }
                finish { phong 1 } 
                rotate<0,0,0> scale 0.5 translate<0,0,0>
              } // end of texture 

} 

plane { <0, -3, 0>, 1
  pigment {
     color White
  }
}

box { 
  <-1, 0, -1>, // Near lower left corner 
  < 1, -1, 3> // Far upper right corner 
           texture{ T_Stone21   
                normal { agate 0.25 scale 0.15 rotate<0,0,0> }
                finish { phong 1 } 
                rotate<0,0,0> scale 0.5 translate<0,0,0>
              } // end of texture 

  rotate y*60 // Equivalent to "rotate <0,60,0>" 
} 
             
sphere { 
  <0, 11, 0>, 1
  texture { 
    pigment { 
      color Yellow 
    } 
  }
}